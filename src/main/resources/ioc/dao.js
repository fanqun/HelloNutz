var ioc = {
		dataSourceDruid : {
			type : "com.alibaba.druid.pool.DruidDataSource",
			events : {
				create : "init",
				depose : 'close'
			},
			fields : {
				//driverClassName : "com.mysql.jdbc.Driver",
				//driverClassName : "org.mariadb.jdbc.Driver",
				url : "jdbc:mysql:///nutz",
				//url : "jdbc:mariadb://localhost:3306/nutz",
				username : "root",
				password : "root1234",
				maxWait : 15000, // 若不配置此项,如果数据库未启动,druid会一直等可用连接,卡住启动过程,
				defaultAutoCommit : false,		// 提高fastInsert的性能
	            testWhileIdle : true,
	            validationQuery : "select 1",
	            initialSize : 10,
	            maxActive : 20,
	            minIdle : 5,
	            poolPreparedStatements : true,
	            filters : "mergeStat",
	            connectionProperties : "druid.stat.slowSqlMillis=2000",
			}
		} ,
		dao : {
			type : "org.nutz.dao.impl.NutDao",
			args : [ {
				refer : "dataSourceDruid"
			} ]
		}
};