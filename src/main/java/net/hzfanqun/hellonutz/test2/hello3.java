package net.hzfanqun.hellonutz.test2;

import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

@At("/h3")
@Ok(">>:/h5/h1")
public class hello3 {

	@At("/h1")
	public String hello(){
		return "hello3-hello";
	}

}
