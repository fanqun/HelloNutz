package net.hzfanqun.hellonutz.test2;

import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

@At("/h5")
@Ok("->:/pet.jsp")
public class hello5 {
	
	@At("/h1")
	public String hello(){
		return "hello5-hello";
	}

}
