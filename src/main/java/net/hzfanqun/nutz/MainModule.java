package net.hzfanqun.nutz;

import net.hzfanqun.hellonutz.test1.hello2;
import net.hzfanqun.hellonutz.test2.hello3;
import org.nutz.mvc.annotation.*;

@Encoding(input = "utf-8", output = "utf-8")
@Modules(value = {hello2.class, hello3.class}, scanPackage = true)
@SetupBy(MainSetup.class)
@Fail("json")
@UrlMappingBy(value = UrlMappingSet.class)

// 不建议jason的配置文件直接放在classpath目录下，此处放在classpath目录的IOC目录
@IocBy(args = {"*js", "ioc/", "*anno", "net.hzfanqun.nutz.demo", "net.hzfanqun.nutz.module"})
public class MainModule {
}
