package net.hzfanqun.nutz.module;

import java.io.File;

import org.nutz.ioc.loader.annotation.Inject;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.lang.Files;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

import net.hzfanqun.nutz.bean.Pet;

@IocBean
@At("/test")
public class TestNutzModule {

	@Inject("refer:xiaobai")
	private Pet pet;

	@At("/h1")
	@Ok("json")
	public Object Hello1(){
		String str="abcd";
		return pet;
	}
	
	@At
	@Ok("raw")
	public File down() {
		return Files.findFile("E:/libMonitor.bak");
	}

}
