package net.hzfanqun.nutz.demo;

import org.nutz.ioc.Ioc;
import org.nutz.ioc.impl.NutIoc;
import org.nutz.ioc.loader.annotation.IocBean;
import org.nutz.ioc.loader.json.JsonLoader;
import org.nutz.mvc.annotation.At;
import org.nutz.mvc.annotation.Ok;

import net.hzfanqun.nutz.bean.Pet;

@IocBean
public class HelloPet {

	@At("/hellopet")
	@Ok("jsp:/pet.jsp")
	/**
	 * 
	 * @param ioc
	 * @return 返回值会保存在request的attribute,名字是obj
	 */
	public String hello(Ioc ioc){
		Pet pet = ioc.get(Pet.class, "xiaobai");
		System.out.printf("%s - [%s]\n", pet.getName(), pet.getBirthday().getTimeZone().getID());
		Pet xh = ioc.get(null, "xiaohei");
		System.out.printf("%s's friend is %s\n", xh.getName(), xh.getFriend().getName());
		return String.format("%s - [%s]\n", pet.getName(), pet.getBirthday().getTimeZone().getID());
	}
	
	public static void main(String[] args) {
		Ioc ioc = new NutIoc(new JsonLoader("pets.js"));
		Pet pet = ioc.get(Pet.class, "xiaobai");
		System.out.printf("%s - [%s]\n", pet.getName(), pet.getBirthday().getTimeZone().getID());
		
		Pet xh = ioc.get(null, "xiaohei");
		System.out.printf("%s's friend is %s\n", xh.getName(), xh.getFriend().getName());
		
		ioc.depose(); // 关闭ioc容器.
	}

}
