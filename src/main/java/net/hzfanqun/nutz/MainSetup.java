package net.hzfanqun.nutz;

import org.nutz.mvc.NutConfig;
import org.nutz.mvc.Setup;
import org.nutz.mvc.view.UTF8JsonView;

public class MainSetup implements Setup {

	@Override
	public void init(NutConfig nc) {
		 UTF8JsonView.CT = "text/plain";
	}

	@Override
	public void destroy(NutConfig nc) {
	}

}
