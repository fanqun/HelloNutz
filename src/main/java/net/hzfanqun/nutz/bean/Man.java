package net.hzfanqun.nutz.bean;

import org.nutz.dao.entity.annotation.Column;  
import org.nutz.dao.entity.annotation.Name;  
import org.nutz.dao.entity.annotation.Table;  
  
@Table("t_man")   // 声明了Man对象的数据表  
public class Man {  
      
    public Man() {}  
      
    @Column  
    @Name    // 表示该字段可以用来标识此对象，或者是字符型主键，或者是唯一性约束  
    private String name;  
    @Column  
    private String age;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}  


}  