<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script type="text/javascript"></script>
    </head>
    <body>
      <form id="formtest" action="yousay" method="get">
        <p><span>输入姓名:</span><input type="text" name="username" id="username" /></p>
        <p><span>输入年龄:</span><input type="text" name="age" id="age" /> </p>
        <input   type=submit   value= "test ">
      </form>
      <br /><br />
      <div>服务器用request设定的值：</div>
      <div>姓名_年龄-----<%=request.getAttribute("username_age")%></div>
      <br />
      <div>服务器返回结果：</div>
      <div id="result_sv"><%=request.getAttribute("obj")%></div>
      <br />
      <div>对服务器的json形式的返回结果，用js进行解析：</div>
      <div id="result"></div>
      <script type="text/javascript">
        var rs=document.getElementById("result_sv").innerHTML;
        if (rs){
            eval("json=" + rs + ";");
            var str="姓名:"+json.username+"<br />";
            str+="年龄:"+json.age+"<br />";
            str+="追加测试:"+json.append;
            document.getElementById("result").innerHTML=str;
        }
      </script>
    </body>
</html>
