<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Fetch Origin ID Page</title>
        <script type="text/javascript"></script>
    </head>
    <body>
      <form id="formtest" action="compret" method="post">
        <p><span>输入错误的ID(负数):</span>&nbsp;&nbsp;<input type="text" name="errorid" id="errorid" /></p>
        <input   type="submit"   value= "comp">
      </form>
      <br /><br />
      <div></div>
      <div>输入错误的ID(负数) <%=request.getAttribute("errorid")%></div>
      <br />
      <div>返回结果：</div>
      <div id="result_sv"><%=request.getAttribute("obj")%></div>
      <br />
      <div>对服务器的json形式的返回结果，用js进行解析：</div>
      <div id="result"></div>
      <script type="text/javascript">
        var rs=document.getElementById("result_sv").innerHTML;
        if (rs){
            eval("json=" + rs + ";");
            var str="错误的ID:  "+json.error_id+"<br />";
            str+="原始ID:  "+json.origin_id+"<br />";
            str+="追加者:  "+json.append;
            document.getElementById("result").innerHTML=str;
        }
      </script>
    </body>
</html>
